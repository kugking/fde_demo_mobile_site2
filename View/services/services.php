<!-- service start -->
<div data-role="page" id="services">
	<? include("home/header.php"); ?>
	<div data-role="content">
		<div class="h2-css" >Services</div>

		<ul  data-role="listview" data-theme="" data-inset="true">
			<li><a class="char-black" href="#newCollection">New Collection</a></li>
			<li><a class="char-black" href="#foodMenu">Food Menu</a></li>
			<li><a class="char-black" href="#coffeeMenu">Coffee Menu</a></li>
		</ul>

	</div>

</div>
<!-- service end-->