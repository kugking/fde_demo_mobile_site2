
<html>
<head>
	<title>Front Desk Easy</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Theme roller -->
	<link rel="stylesheet" href="../Public/themes/FDE_Theme_Black_and_Green.min.css" />
	<!--  -->
	<link rel="stylesheet" href="http://code.jquery.com/mobile/1.3.1/jquery.mobile.structure-1.3.1.min.css" />
	<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
	<script src="http://code.jquery.com/mobile/1.3.1/jquery.mobile-1.3.1.min.js"></script>
	<link rel="stylesheet" href="../Public/CSS/img-button-css.css" />

	<!-- bxSlider Javascript file -->
	<script src="../Public/JavaScripts/jquery.bxslider.min.js"></script>
	<!-- bxSlider CSS file -->
	<link href="../Public/CSS/jquery.bxslider.css" rel="stylesheet" />

	<!-- Script BXSlider Start -->
	<script type="text/javascript">
	$(document).ready(function(){
		$('.bxslider').bxSlider();
	});
	</script>
	<!-- Script BXSlider End -->

	<style type="text/css" media="screen">
		
	</style>



</head>
<body class="body-black">

	<!-- include file start -->
	<? include("home/home.php"); ?>
	<? include("home/homeInfo.php"); ?>
	<? include("room/room.php"); ?>
	
	<? include("booking/booking.php"); ?>
	<? include("booking/yourBooking.php"); ?>
	<? include("booking/GuestDetail.php"); ?>
	<? include("booking/confrimBooking.php"); ?>
	<? include("booking/checkAvailability.php"); ?>
	<? include("services/services.php"); ?>
	<? include("services/coffeeMenu.php"); ?>
	<? include("services/foodMenu.php"); ?>
	<? include("services/newCollection.php"); ?>
	<? include("room/roomDetail.php"); ?>
	
	<!-- include file end -->
</body>
</html>


